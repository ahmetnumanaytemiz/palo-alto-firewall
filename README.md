#### Palo Alto Firewall Notes

> Ahmet Numan Aytemiz, 28.02.2021

---

#### 1. Palo Alto Firewall Getting Started

- Understanding firewall basics and Palo Alto's Advantages
- Initializing the Palo Alto
- Administering the firewall
- Deploying Palo Alto in layer 3 mode
- Processing Palo Alto Traffic
- Seeting up security rules to shape traffic

![Image](images/palo_lab.png)