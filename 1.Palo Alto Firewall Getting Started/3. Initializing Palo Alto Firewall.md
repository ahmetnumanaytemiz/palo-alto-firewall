#### Initializing Palo Alto Firewall

> Ahmet Numan Aytemiz , 10 February 2021

---

- Use case and network diagram
- Initial Configuration
- Graphical User Interface
- Management Settings
- Configuration Management

https://docs.paloaltonetworks.com/pan-os/9-0/pan-os-admin.html (choose version)