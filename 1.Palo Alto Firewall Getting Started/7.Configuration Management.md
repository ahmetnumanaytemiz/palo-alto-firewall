#### Configuration Management

> Ahmet Numan Aytemiz, 10 February 2021

---

#### We can take configuration snapshot to save config file to use later...

**Device > Setup > Operaitons > Save > Save named configuration snapshot**

![Image](images/save.PNG)


#### We can export saved config snapshot

![Image](images/export.PNG)

#### we can import exported config...

![Image](images/import.PNG)

Note: Use master key to encrypt passwords in the config file, if you use this option dont forget master key!

**Device > Master Key and diagnostics**

---
